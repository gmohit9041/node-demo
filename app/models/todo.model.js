const mongoose = require('mongoose');
const TodoSchema = mongoose.Schema({
	user_id:String,
    title: String,
    description: String,
}, {
    timestamps: true
});

module.exports = mongoose.model('Todo', TodoSchema);
