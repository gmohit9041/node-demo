const { body } = require('express-validator')
const express = require('express')
const router =  express.Router();
const todos = require('../controllers/todo.controller.js');
const authMiddleware = require('../middleware/jwtAuth.js');
//Add Middleware
router.use(authMiddleware.auth);
//List all Todos
router.get('/todos',todos.findAll);
router.post('/todos', [
	body('title').notEmpty().withMessage('Please insert title'),
	body('description').notEmpty().withMessage('Please insert description'),
] , todos.add);
router.put('/todos/:id', [
	body('title').notEmpty().withMessage('Please insert title'),
	body('description').notEmpty().withMessage('Please insert description'),
], todos.update);
router.delete('/todos/:id', todos.delete);
	

module.exports = router;
    
