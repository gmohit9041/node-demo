const authRoute = require('./auth.routes.js');
const todoRoute = require('./todo.routes.js');

module.exports = (app) => {
		app.use(authRoute);
		app.use(todoRoute);
}
    

    
