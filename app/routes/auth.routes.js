const { body } = require('express-validator')
const User = require('../models/users.model.js');
var express = require('express')
var router =  express.Router();
var auth = require('../controllers/auth.controller.js');
    
router.post('/login',function() {
	return [
	body('email').isEmail().withMessage('please enter valid email address'),
	body('password').notEmpty().withMessage('please insert valid password'),
] }(),auth.login); 

router.post('/register',function() {
	return [
	body('name').notEmpty().withMessage('please insert valid name'),
    body('email').isEmail().withMessage('please enter valid email address').bail().custom(value=>{
        return User.findOne({email:value}).then(user=>{
            if (user) {
					return Promise.reject('E-mail already in use');
                }
            })
        }).withMessage('please enter valid email address'),
    body('password').isLength({ min: 8 }).withMessage('password should have 8 character'),
] }(),auth.register); 

module.exports = router;


    

    
