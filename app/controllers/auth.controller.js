const User = require('../models/users.model.js');
const {  validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');

exports.register = async (req,res) => {
    const errors = validationResult(req)

    if(!errors.isEmpty()) {
        res.json({status:'error',errors:errors},422)
		return;
    } 
	// Create a User
	const note = new User({
		name: req.body.name, 
		email: req.body.email,
		password:req.body.password,
	});
	await note.save()
	.then(data => {
		res.json({status:'success',data:data,message:'User Register Successfully'},200);
	}).catch(err => {
		res.status(500).json({status:'success',message: err.message || "Some error occurred while creating the Note."});
	});
}

exports.login = async function(req,res)  {
    const errors = validationResult(req)
	
    if(!errors.isEmpty()) {
		res.json({status:'error',errors:errors},422)
		return; 
    }
	User.findOne({email:req.body.email,password:req.body.password})
        .then(user => {
            if(!user) {
				let loginError = { 
								errors:
										[
											{
												msg:'User Email And Password Not Exist in System'
											}
										]
								 };
                res.json({status:'error',errors:loginError},422); 
            }else{
				const token = jwt.sign({id: user._id}, req.app.get('secretKey'), { expiresIn: '24h' });
                res.json({status:'success',data:user,access_token:token,message:'User Login Successfully'},200);
            }
        }).catch(err => {
			res.status(500).json({status:'success',message: err.message || "Some error occurred while creating the Note."});
        });
}
