const Todo = require('../models/todo.model.js');
const {  validationResult } = require('express-validator');
// Create and Save a new Note

// Retrieve and return all notes from the database.
exports.findAll = (req, res) => {
    Todo.find({user_id:req.body.userId})
    .then(todos => {
        res.json({status:'success',data:todos,message:'List of all todos'},200)
    }).catch(err => {
		 res.json({status:'error',message:err.message || "Some error occurred while retrieving notes."},500)
    });
};

exports.add = (req, res,next) => {
	
    // Validate request
    const errors = validationResult(req)
	
    if(!errors.isEmpty()) {
        res.json({status:'error',errors:errors},422)
		return; 
    }
	
	// Create a Todo
	const todo = new Todo({
		user_id:req.body.userId,
		title: req.body.title, 
		description: req.body.description,
	});
	todo.save()
	.then(todo => {
		res.json({status:'success',data:todo,message:'Todo insert successfully'},200)
	}).catch(err => {
		res.json({status:'error',message:err.message || "Some error occurred while retrieving notes."},500)
	});
};

exports.update = (req, res) => {
    // Validate Request
    const errors = validationResult(req)
	
	if(!errors.isEmpty()) {
        res.json({status:'error',errors:errors},422)
		return; 
    }
	let a =5;
    // Find todo and update it with the request body
	Todo.findOneAndUpdate({_id:req.params.id,user_id:req.body.userId}, {
		title: req.body.title,
		description: req.body.description
	},{new: true} )
	.then(todo => {
		if(!todo) {
			res.json({status:'error',message:"Record not exist in system"},404)
		}
		a= a+1;
		console.log(a);
		res.json({status:'success',data:todo,message:'Todo update successfully'},200)
	}).catch(err => {
		res.json({status:'error',message:err.message || "Some error occurred while retrieving notes."},500)
	});
    
};

exports.delete = (req, res) => {
    Todo.findOneAndRemove({_id:req.params.id,user_id:req.body.userId})
    .then(todo => {
        if(!todo) {
           res.json({status:'error',message:"Record not exist in system"},404)
        }
        res.json({status:'success',data:todo,message:'Todo Delete successfully'},200)
    }).catch(err => {
        res.json({status:'error',message:err.message || "Some error occurred while retrieving notes."},500)
    });
};













