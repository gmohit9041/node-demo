const express = require('express');
//get Data from form
const bodyParser = require('body-parser');
//For upload files
const fileUpload = require('express-fileupload');
//Database Package
const mongoose = require('mongoose');
//Load Environment Variable
require('dotenv').config()


//Initialize Express
const app = express();
// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))
// parse requests of content-type - application/json
app.use(bodyParser.json())
//parse requests of content-type - application/formdata
app.use(fileUpload());

//Define JWT Secret Key
app.set('secretKey', '1ec34a');


mongoose.connect(process.env.DB_URL, {
  useNewUrlParser: true,
  useUnifiedTopology: true
}).then(()=>{
	console.log('Successfully Connected To Database.......')
}).catch(err=>{
	console.log('Not Connected To Database.......' + err)
});


// define a simple route
app.get('/', async (req, res) => {
    res.send({status:'success',message:'Welcome to Node Todo App API'},200)
});

require('./app/routes/routes.js')(app);

// listen for requests 
app.listen(process.env.PORT, () => {
    console.log("Server is listening on port 3000");
});